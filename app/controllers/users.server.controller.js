var User = require('../models/user.server.model'),
    passport = require('passport');

var getErrorMessage = function (err) {
    var message = '',
        errName;

    if (err.code) {
        switch (err.code) {
            case 11000:
            case 11001:
                message = 'Username already exists';
                break;

            default:
                message = 'Something went wrong';
        }
    } else {
        for (errName in err.errors) {
            if (err.errors[errName].message) {
                message = err.errors[errName].message;
            }
        }
    }
    return message;
};

exports.renderLogin = function (req, res, next) {
    if (!req.user) {
        res.render('login', {
            title: 'Login Form',
            messages: req.flash('error') || req.flash('info')
        });
    } else {
        return res.redirect('/');
    }
};

exports.renderRegister = function (req, res, next) {
    if (!req.user) {
        res.render('register', {
            title: 'Register Form',
            messages: req.flash('error')
        });
    } else {
        return res.redirect('/');
    }
};

exports.register = function (req, res, next) {
    if (!req.user) {
        var user = new User(req.body);
        var message = null;

        user.provider = 'local';

        user.save(function (err) {
            if (err) {
                var message = getErrorMessage(err);

                req.flash('error', message);
                return res.redirect('/register');
            }

            req.login(user, function (err) {
                if (err) return next(err);

                return res.redirect('/');
            });
        });
    } else {
        return res.redirect('/');
    }
};

exports.logout = function (req, res) {
    req.logout();
    res.redirect('/');
}

exports.create = function (req, res, next) {
    var user = new User(req.body);

    user.save(function (err) {
        if (err) {
            return next(err);
        }
        res.json(user);
    });
};

exports.list = function (req, res, next) {
    User.find({}, function (err, users) {
        if (err) {
            return next(err);
        }

        res.json(users);
    });
};

exports.read = function (req, res, next) {
    User.findByID(req.params.id, function (err, user) {
        if (err) {
            return next(err);
        }

        res.json(user);
    });
};

exports.update = function (req, res, next) {
    User.findByIdAndUpdate(req.params.id, req.body, function (err, user) {
        if (err) {
            return next(err);
        }

        res.json({
            success: "User was updated",
            newUser: user
        });
    });
};

exports.delete = function (req, res, next) {
    User.remove({_id: req.params.id}, function (err) {
        if (err) {
            return next(err);
        }
        console.log('Removed Yo!!');
        res.json({
            success: "Removed User from DB!!"
        });
    });
}
