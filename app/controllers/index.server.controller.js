exports.render = function (req, res) {
    // Session storage: req.session
    // record the time of last user request - to test session

    res.render('index', {
        title: 'Hello World',
        userFullName: req.user ? req.user.name : ''
    });
};
