var mongoose = require('mongoose'),
    crypto = require('crypto'),
    Schema = mongoose.Schema;

var UserSchema = new Schema({
    name: String,
    email: {
        type: String,
        required: true,
        index: true,
        match: [/.+\@.+\..+/, "Please fill a valid e-mail address"]
    },
    username: {
        type: String,
        required: true,
        unique: true,
        match: /^[a-zA-Z_0-9]+$/
    },
    password: {
        type: String,
        validate: [
            function (password) {
                return password && password.length >= 8
            }, 'password should be longer'
        ]
    },
    salt: String,
    provider: {
        type: String,
        required: 'Provider is required'
    },
    providerId: String,
    providerData: {},
    created: {
        type: Date,
        default: Date.now
    },
    website: {
        type: String,
        set: function (url) {
            if (!url) return url;

            if (url.indexOf('http://') !== 0 && url.indexOf('https://') !== 0) {
                return 'http://' + url;
            }
            return url;
        }
    }
});

UserSchema.virtual('loginInfo').get(function () {
    return 'Username: ' + this.username + ' password: ' + this.password;
});

// to execute your custom getter when MongoDB document are converted to
// JSON representation
// UserSchema.set('toJSON', { getters: true, virtuals: true });
UserSchema.set('toJSON', { virtuals: true });

// Static Methods on Schema
UserSchema.statics.findByID = function (id, cb) {
    var self = this;
    self.findOne({ _id: id }, cb);
};

UserSchema.statics.findUniqueUsername = function (username, suffix,
    callback) {
    var self = this;
    var possibleUsername = username + (suffix || '');
    self.findOne({
        username: possibleUsername
    }, function (err, user) {
        if (!err) {
            if (!user) {
                callback(possibleUsername);
            } else {
                return self.findUniqueUsername(username, (suffix || 0) +
                    1, callback);
            }
        } else {
            callback(null);
        }
    });
};

// Instance Methods
UserSchema.methods.authenticate = function (password) {
    return this.password === this.hashPassword(password);
};

UserSchema.methods.hashPassword = function (password) {
    return crypto.pbkdf2Sync(password, this.salt, 10000, 64).toString('base64');
};

// pre + post middleware of Mongoose schema
UserSchema.pre('save', function (next) {
    if (this.password) {
        this.salt = new Buffer(crypto.randomBytes(16).toString('base64'), 'base64');
        this.password = this.hashPassword(this.password);
    }
    next();
});


var User = mongoose.model('User', UserSchema);

module.exports = User;
